import Vue from 'vue'

new Vue({
  el: '#app',
  data: {
    message: 'Vue instance it\'s working better!'
  },
  methods: {
    demo: function () {
      alert('Method triggered! >:(')
    }
  }
})
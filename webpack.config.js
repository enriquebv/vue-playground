const path = require('path')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

const config = {
  entry: path.resolve(__dirname, 'src/index.js'),
  output: {
    filename: 'build.js',
    path: path.resolve(__dirname, 'dist')
  },
  resolve: {
    alias: {
      'vue': 'vue/dist/vue.esm.js'
    }
  }
}

const minimizeOptions = {
  parallel: true,
  uglifyOptions: {
    output: {
      comments: false,
      beautify: false
    }
  }
}

module.exports = (env, argv) => {
  if (argv.mode === 'development') {
    config.devtool = 'source-map'
  }

  if (argv.mode === 'production') {
    config.optimization = {
      minimize: true,
      minimizer: [new UglifyJsPlugin(minimizeOptions)]
    }
  }

  return config
}
